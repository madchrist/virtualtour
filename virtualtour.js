/*
Virtualtour v0.4

Copyright © 2015 madx

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  TODO
  - v0.5
    - match rotation with mouse position (?)
  - v0.6
    - arbitrary overlay of pictures
  - v0.7
    - interface
  - v0.8
    - streetview-like changement of location
  - v0.9
    - touch support
    - gyroscope support
  - v?.?
    - equirectangular support (is it even possible without really heavy computation ?)
*/

/*
  Required:
  - jQuery 2+
  - jQuery-mousewheel 3+
  - sylvester.js (http://sylvester.jcoglan.com)


  The panoramic image must be a cubic projection:

  XXXXXX|      |XXXXXX|XXXXXX
  XXXXXX| TOP  |XXXXXX|XXXXXX
  XXXXXX|      |XXXXXX|XXXXXX
  ------+------+------+------
        |      |      |
   LEFT |FRONT |RIGHT | BACK
        |      |      |
  ------+------+------+------
  XXXXXX|      |XXXXXX|XXXXXX
  XXXXXX|BOTTOM|XXXXXX|XXXXXX
  XXXXXX|      |XXXXXX|XXXXXX

  All 6 faces must be squares of equal sizes

  Performance will be slightly higher if the squares' side is a power of two (because mipmapping requires it)


  Misc:
  jQuery is only used for events (so that they work with all browsers)
*/



var virtualtours = {};
var virtualtour_options_default = { world_yaw:0, world_pitch:0*Math.PI/180, world_roll:0, vertical_fov:70 };


function virtualtour(canvasId, filename, options)
{
    // create a new object for this instance
    virtualtours[canvasId] = {};
    var vt = virtualtours[canvasId];
    vt.models = { };

    // options, set missing option to its default value
    vt.options = virtualtour_options_default;
    for(var i in options) { vt.options[i] = options[i]; }
    
    // find canvas
    vt.canvasId = canvasId;
    vt.canvas = document.getElementById(canvasId);
    if(vt.canvas == null) { console.warn('Could not find the canvas, aborting'); return; }

    // initialize WebGL
    virtualtour_initWebgl(vt);

    // initialize View and Projection matrices
    vt.view = { yaw:0, pitch:0, roll:0 };
    virtualtour_updateViewMatrix(vt);
    vt.projection = { znear:0.1, zfar:100, vfov:vt.options.vertical_fov };
    virtualtour_updateProjectionMatrix(vt);

    // resize webgl viewport to fit canvas
    virtualtour_resize(vt);

    // load the cube in the background
    var img = document.createElement('img');
    img.onload = function() { virtualtour_cubeAdd(vt, img, vt.options.world_yaw, vt.options.world_pitch, vt.options.world_roll); virtualtour_draw(vt); }
    img.src = filename;

    var img2 = document.createElement('img');
    img2.onload = function()
    {
        vt.models.arrow = virtualtour_modelCreate(vt, virtualtour_rotationMatrix(-1.778, 0.194, 1.6), [ .1,.1,-.9, .1,-.1,-.9, -.1,-.1,-.9, -.1,.1,-.9 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], img2);
        virtualtour_draw(vt);
    }
    img2.src = 'arrow.png';

    
    // initialize events handlers
    vt.mouse = { lastX:null, lastY:null };
    $(vt.canvas).on('mousedown.vt.' + vt.canvasId, function(event) { virtualtour_mousedown(vt, event.pageX, event.pageY); });
    $(vt.canvas).on('mousewheel.vt.' + vt.canvasId, function(event) { virtualtour_mousewheel(vt, event.deltaX, event.deltaY); virtualtour_draw(vt); });
    $(window).on('resize.vt.' + vt.canvasId, function(event) { virtualtour_resize(vt); virtualtour_draw(vt); });

    // draw first image (although it should be empty at this point)
    virtualtour_draw(vt);
}


function virtualtour_initWebgl(vt)
{
    // get webgl context
    vt.gl = vt.canvas.getContext("webgl") || vt.canvas.getContext("experimental-webgl");
    if(!vt.gl) { alert("Unable to initialize WebGL. Your browser may not support it."); return; }

    // clear everything
    vt.gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    vt.gl.clearDepth(1.0);                 // Clear depth buffer
    vt.gl.enable(vt.gl.DEPTH_TEST);        // Enable depth testing FIXME: no effect ? already default ?
    vt.gl.depthFunc(vt.gl.LEQUAL);         // Near things obscure far things

    // activate blending (i.e. transparent textures)
    vt.gl.enable(vt.gl.BLEND);
    vt.gl.blendEquation(vt.gl.FUNC_ADD);
    vt.gl.blendFunc(vt.gl.SRC_ALPHA, vt.gl.ONE_MINUS_SRC_ALPHA);
    
    // create shader program
    vt.shaderProgram = vt.gl.createProgram();

    // add shaders to shader program
    var shaders = { fragment:{ source:virtualtour_fragment_shader_source, type:vt.gl.FRAGMENT_SHADER}, vertex:{ source:virtualtour_vertex_shader_source, type:vt.gl.VERTEX_SHADER} };
    for(var i in shaders)
    {
        var shader = vt.gl.createShader(shaders[i].type);
        vt.gl.shaderSource(shader, shaders[i].source);

        // compile shader
        vt.gl.compileShader(shader);
        if (!vt.gl.getShaderParameter(shader, vt.gl.COMPILE_STATUS)) { alert("An error occurred compiling the shaders: " + vt.gl.getShaderInfoLog(shader)); }
        // FIXME fail handling
        vt.gl.attachShader(vt.shaderProgram, shader);
    }

    // more shader program init
    vt.gl.linkProgram(vt.shaderProgram);
    if(!vt.gl.getProgramParameter(vt.shaderProgram, vt.gl.LINK_STATUS)) { alert("Failed to create shader program."); return; }
    vt.gl.useProgram(vt.shaderProgram);

    // fetch locations of glsl variables
    vt.shaderVars = {};
    vt.shaderVars.aModelVertexPosition = vt.gl.getAttribLocation( vt.shaderProgram, "aModelVertexPosition");
    vt.shaderVars.uModelMatrix         = vt.gl.getUniformLocation(vt.shaderProgram, "uModelMatrix"        );
    vt.shaderVars.uViewMatrix          = vt.gl.getUniformLocation(vt.shaderProgram, "uViewMatrix"         );
    vt.shaderVars.uProjectionMatrix    = vt.gl.getUniformLocation(vt.shaderProgram, "uProjectionMatrix"   );
    vt.shaderVars.aTextureCoord        = vt.gl.getAttribLocation( vt.shaderProgram, "aTextureCoord"       );
    vt.shaderVars.uSampler             = vt.gl.getUniformLocation(vt.shaderProgram, "uSampler"            );
    vt.gl.enableVertexAttribArray(vt.shaderVars.aModelVertexPosition);
    vt.gl.enableVertexAttribArray(vt.shaderVars.aTextureCoord);
}


function virtualtour_updateViewMatrix(vt)
{
    vt.gl.uniformMatrix4fv(vt.shaderVars.uViewMatrix, false, new Float32Array(virtualtour_rotationMatrix(vt.view.yaw, vt.view.pitch, vt.view.roll).flatten()));
}


function virtualtour_updateProjectionMatrix(vt)
{
    var aspect = vt.canvas.clientWidth/vt.canvas.clientHeight;
    var znear = vt.projection.znear;
    var zfar = vt.projection.zfar;
    var ymax = znear * Math.tan(vt.projection.vfov * Math.PI / 360.0);
    var ymin = -ymax;
    var xmin = ymin * aspect;
    var xmax = ymax * aspect;

    var X = 2*znear/(xmax-xmin);
    var Y = 2*znear/(ymax-ymin);
    var A = (xmax+xmin)/(xmax-xmin);
    var B = (ymax+ymin)/(ymax-ymin);
    var C = -(zfar+znear)/(zfar-znear);
    var D = -2*zfar*znear/(zfar-znear);

    var matrix = $M([[X, 0,  A, 0],
                     [0, Y,  B, 0],
                     [0, 0,  C, D],
                     [0, 0, -1, 0]]);

    vt.gl.uniformMatrix4fv(vt.shaderVars.uProjectionMatrix, false, new Float32Array(matrix.flatten()));
}


function virtualtour_resize(vt)
{
    var width = vt.canvas.clientWidth;
    var height = vt.canvas.clientHeight;
    if (vt.canvas.width != width || vt.canvas.height != height)
    {
        vt.canvas.width = width;
        vt.canvas.height = height;
        vt.gl.viewport(0, 0, width, height);
    }

    // update projection matrix
    virtualtour_updateProjectionMatrix(vt)
}


function virtualtour_mousedown(vt, x, y)
{
    vt.mouse.lastX = x;
    vt.mouse.lastY = y;

    vt.canvas.style.cursor = 'move';
    $(document).on('mousemove.vt.' + vt.canvasId, function(event) { virtualtour_mousemove(vt, event.clientX, event.clientY); virtualtour_draw(vt); });
    $(document).on('mouseup.vt.' + vt.canvasId, function() { $(document).off('mousemove.vt.' + vt.canvasId); $(document).off('mouseup.vt.' + vt.canvasId); vt.canvas.style.cursor = 'default'; });
}


function virtualtour_mousemove(vt, x, y)
{
    // update yaw and pitch
    vt.view.yaw -= (x - vt.mouse.lastX)/1000;
    vt.view.pitch -= (y - vt.mouse.lastY)/1000;

    // limit pitch to +90°/-90°
    if(vt.view.pitch >  Math.PI/2) { vt.view.pitch =  Math.PI/2; }
    if(vt.view.pitch < -Math.PI/2) { vt.view.pitch = -Math.PI/2; }

    vt.mouse.lastX = x;
    vt.mouse.lastY = y;

    //update view matrix
    virtualtour_updateViewMatrix(vt);
}

function virtualtour_mousewheel(vt, x, y)
{
    // update vertical FOV
    vt.projection.vfov *= 1-(y/20);

    // limit vertical FOV to 90° to avoid weirdness
    if(vt.projection.vfov > 90) { vt.projection.vfov = 90; }

    // update projection matrix
    virtualtour_updateProjectionMatrix(vt);
}



function virtualtour_rotationMatrix(yaw, pitch, roll)
{
    var c1 = Math.cos(yaw);
    var s1 = Math.sin(yaw);
    var c2 = Math.cos(pitch);
    var s2 = Math.sin(pitch);
    var c3 = Math.cos(roll);
    var s3 = Math.sin(roll);

    // y-x'-z" rotation matrix (euler angles, intrinsic), this one gives correct yaw/pitch/roll in opengl's axes (Y up) (equivalent to z-y'-x" with standard cartesian axes (Z up))
    // Matches Z1X2Y3 from https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix but with 1 and 3 switched
    return($M([[ c1*c3-s1*s2*s3, -c2*s3, c3*s1+c1*s2*s3, 0 ],
               [ c1*s3+c3*s1*s2,  c2*c3, s1*s3-c1*c3*s2, 0 ],
               [    -c2*s1     ,    s2 ,     c1*c2     , 0 ],
               [       0       ,    0  ,       0       , 1 ]]));
}



function virtualtour_draw(vt)
{
    // debug
    console.log('yaw=' + vt.view.yaw + ' pitch=' + vt.view.pitch + ' roll=' + vt.view.roll + ' vfov=' + vt.projection.vfov);
    
    // clear everything
    vt.gl.clear(vt.gl.COLOR_BUFFER_BIT | vt.gl.DEPTH_BUFFER_BIT);

    // draw all models
    for(i in vt.models)
    {
        if(i.substring(0,4) == 'cube_') { virtualtour_modelDraw(vt, vt.models[i]); }
    }
    for(i in vt.models)
    {
        if(i.substring(0,4) != 'cube_') { virtualtour_modelDraw(vt, vt.models[i]); }
    }

}



function virtualtour_cubeAdd(vt, img, yaw, pitch, roll)
{
    // create the model matrix
    matrix = virtualtour_rotationMatrix(yaw, pitch, roll);
    
    // load the image in a temporary canvas
    var tmpcanvas = document.createElement('canvas');
    tmpcanvas.width = img.width;
    tmpcanvas.height = img.height
    var ctx = tmpcanvas.getContext('2d');
    ctx.drawImage(img, 0, 0);

    // add a model for each of the 6 faces of the cube
    vt.models.cube_top    = virtualtour_modelCreate(vt, matrix, [ -1, 1, 1,  1, 1, 1,  1, 1,-1, -1, 1,-1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(1*img.width/4, 0*img.height/3, img.width/4, img.height/3));
    vt.models.cube_left   = virtualtour_modelCreate(vt, matrix, [ -1, 1, 1, -1, 1,-1, -1,-1,-1, -1,-1, 1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(0*img.width/4, 1*img.height/3, img.width/4, img.height/3));
    vt.models.cube_front  = virtualtour_modelCreate(vt, matrix, [ -1, 1,-1,  1, 1,-1,  1,-1,-1, -1,-1,-1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(1*img.width/4, 1*img.height/3, img.width/4, img.height/3));
    vt.models.cube_right  = virtualtour_modelCreate(vt, matrix, [  1, 1,-1,  1, 1, 1,  1,-1, 1,  1,-1,-1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(2*img.width/4, 1*img.height/3, img.width/4, img.height/3));
    vt.models.cube_back   = virtualtour_modelCreate(vt, matrix, [  1, 1, 1, -1, 1, 1, -1,-1, 1,  1,-1, 1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(3*img.width/4, 1*img.height/3, img.width/4, img.height/3));
    vt.models.cube_bottom = virtualtour_modelCreate(vt, matrix, [ -1,-1,-1,  1,-1,-1,  1,-1, 1, -1,-1, 1 ], [ 0,1,2, 0,2,3 ], [ 0,0, 1,0, 1,1, 0,1 ], ctx.getImageData(1*img.width/4, 2*img.height/3, img.width/4, img.height/3));
  
    // free some memory, maybe
    ctx = img = null; 
}


/**
 * @brief Create a model
 * @param vt The virtualtour object
 * @param matrix The model matrix
 * @param verticesCoordinates The coordinates of the vertices of the model
 * @param verticesIndices The indices of the vertices to use to create the triangles of the model
 * @param textureCoordinates The texture mapping on the triangles of the model
 * @param texturePixels The pixels of the texture that will be painted on the model
 * @return The model
 */
function virtualtour_modelCreate(vt, matrix, verticesCoordinates, verticesIndices, textureCoordinates, texturePixels)
{
    var model = {};

    // matrix
    model.matrix = matrix;
    
    // vertices coordinates
    model.verticesCoordinatesBuffer = vt.gl.createBuffer();
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, model.verticesCoordinatesBuffer);
    vt.gl.bufferData(vt.gl.ARRAY_BUFFER, new Float32Array(verticesCoordinates), vt.gl.STATIC_DRAW);
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, null);
    
    // vertices indices
    model.verticesIndicesBuffer = vt.gl.createBuffer();
    vt.gl.bindBuffer(vt.gl.ELEMENT_ARRAY_BUFFER, model.verticesIndicesBuffer);
    vt.gl.bufferData(vt.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(verticesIndices), vt.gl.STATIC_DRAW);
    vt.gl.bindBuffer(vt.gl.ELEMENT_ARRAY_BUFFER, null);
    
    // texture coordinates
    model.textureCoordinatesBuffer = vt.gl.createBuffer();
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, model.textureCoordinatesBuffer);
    vt.gl.bufferData(vt.gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), vt.gl.STATIC_DRAW);
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, null);
    
    // texture pixels
    model.texturePixelsBuffer = vt.gl.createTexture();
    vt.gl.bindTexture(vt.gl.TEXTURE_2D, model.texturePixelsBuffer);
    vt.gl.texImage2D(vt.gl.TEXTURE_2D, 0, vt.gl.RGBA, vt.gl.RGBA, vt.gl.UNSIGNED_BYTE, texturePixels);
    vt.gl.texParameteri(vt.gl.TEXTURE_2D, vt.gl.TEXTURE_MAG_FILTER, vt.gl.LINEAR);
    if(virtualtour_isPot(texturePixels.width) && virtualtour_isPot(texturePixels.height))  // mipmaping only works when the width and height are powers of two
    {
        vt.gl.texParameteri(vt.gl.TEXTURE_2D, vt.gl.TEXTURE_MIN_FILTER, vt.gl.LINEAR_MIPMAP_NEAREST);
        vt.gl.generateMipmap(vt.gl.TEXTURE_2D);
    }
    else { vt.gl.texParameteri(vt.gl.TEXTURE_2D, vt.gl.TEXTURE_MIN_FILTER, vt.gl.LINEAR); }
    vt.gl.texParameteri(vt.gl.TEXTURE_2D, vt.gl.TEXTURE_WRAP_S, vt.gl.CLAMP_TO_EDGE); // no horizontal wrapping
    vt.gl.texParameteri(vt.gl.TEXTURE_2D, vt.gl.TEXTURE_WRAP_T, vt.gl.CLAMP_TO_EDGE); // no vertical wrapping
    vt.gl.bindTexture(vt.gl.TEXTURE_2D, null);

    return(model);
}


/**
* @brief Draw a model
* @param vt The virtualtour object where to draw
* @param model The model to draw
*/
function virtualtour_modelDraw(vt, model)
{
    // load modelMatrix
    vt.gl.uniformMatrix4fv(vt.shaderVars.uModelMatrix, false, new Float32Array(model.matrix.flatten()));
    
    // load vertices coordinates
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, model.verticesCoordinatesBuffer);
    vt.gl.vertexAttribPointer(vt.shaderVars.aModelVertexPosition, 3, vt.gl.FLOAT, false, 0, 0);
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, null); 
    
    // load texture coordinates
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, model.textureCoordinatesBuffer);
    vt.gl.vertexAttribPointer(vt.shaderVars.aTextureCoord, 2, vt.gl.FLOAT, false, 0, 0);
    vt.gl.bindBuffer(vt.gl.ARRAY_BUFFER, null);
    
    // load texture pixels
    //vt.gl.activeTexture(vt.gl.TEXTURE0); // useless because it's the default
    vt.gl.bindTexture(vt.gl.TEXTURE_2D, model.texturePixelsBuffer);
    vt.gl.uniform1i(vt.shaderVars.uSampler, 0);
    
    // draw triangles from vertices indices
    vt.gl.bindBuffer(vt.gl.ELEMENT_ARRAY_BUFFER, model.verticesIndicesBuffer);
    vt.gl.drawElements(vt.gl.TRIANGLES, 6, vt.gl.UNSIGNED_SHORT, 0);
    vt.gl.bindBuffer(vt.gl.ELEMENT_ARRAY_BUFFER, null);
    vt.gl.bindTexture(vt.gl.TEXTURE_2D, null); // done at the end because uSampler is just a reference to the first texture of vt.gl and cannot be reset before drawing
}




/**
* @brief Test if a number is a power of two
* @param number The number to test
* @return true|false
*/
function virtualtour_isPot(number) { for(var n = 1; n < number; n *= 2); return(n == number); }



/**
* @brief Small augment for sylvester to convert matrices into a format webgl understands
* Example:
* $M([[11, 12, 13, 14],
*     [21, 22, 23, 24],
*     [31, 32, 33, 34],
*     [41, 42, 43, 44]]).flatten();
* Returns: [11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34, 41, 42, 43, 44]
*
* This is the format expected by webgl's uniformMatrix4fv()
*
* @param this The matrix
* @return The matrix flattened as a 1D array
*/
Matrix.prototype.flatten = function ()
{
    var result = [];
    if (this.elements.length == 0) { return([]); }


    for (var j = 0; j < this.elements[0].length; j++)
    {
        for (var i = 0; i < this.elements.length; i++)
        {
            result.push(this.elements[i][j]);
        }
    }
    return(result);
}


/**
 * @brief Fragment Shader
 * Very simple fragment shader, it only paints the texture on the mesh, no lighting, no shadows
*/
var virtualtour_fragment_shader_source = '\
varying highp vec2 vTextureCoord; \n\
uniform sampler2D uSampler; \n\
\n\
void main(void) \n\
{ \n\
  gl_FragColor = texture2D(uSampler, vTextureCoord); \n\
} \n';


/**
* @brief Vertex Shader
* Very simple vertex shader, it follows the standard Model-View-Projection logic
*/
var virtualtour_vertex_shader_source = '\
attribute vec3 aModelVertexPosition; \n\
uniform mat4 uModelMatrix; \n\
uniform mat4 uViewMatrix; \n\
uniform mat4 uProjectionMatrix; \n\
\n\
attribute vec2 aTextureCoord; \n\
varying highp vec2 vTextureCoord; \n\
\n\
void main(void) \n\
{ \n\
  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aModelVertexPosition, 1.0); \n\
  vTextureCoord = aTextureCoord; \n\
} \n';
