Virtual Tour
====================

360° panorama viewer

Demo
----
[Here](http://madx.org/projects/virtualtour/demo)

Features
--------
* Cubemaps
* Mouse dragging to rotate to view
* Mouse wheel to zoom

Requirements
------------
* jquery 2+
* jquery-mousewheel 3+
* sylvester.js
* A browser with WebGL support

Install
-------
TODO / See demo directory

